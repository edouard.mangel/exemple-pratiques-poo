﻿using System.Runtime.Serialization;

namespace Domaine;

[Serializable]
internal class BatterieVideException : Exception
{
    public BatterieVideException()
    {
    }

    public BatterieVideException(string? message) : base(message)
    {
    }

    public BatterieVideException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected BatterieVideException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}