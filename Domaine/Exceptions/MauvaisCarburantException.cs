﻿using System;
using System.Runtime.Serialization;

namespace Domaine;

[Serializable]
internal class MauvaisCarburantException : Exception
{
    public MauvaisCarburantException()
    {
    }

    public MauvaisCarburantException(string? message) : base(message)
    {
    }

    public MauvaisCarburantException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected MauvaisCarburantException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}