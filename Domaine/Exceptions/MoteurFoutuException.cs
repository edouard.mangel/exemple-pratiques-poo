﻿using System;
using System.Runtime.Serialization;

namespace Domaine;

// Code généré par l'IDE à l'héritage de la classe Exception
[Serializable]
internal class MoteurFoutuException : Exception
{
    public MoteurFoutuException()
    {
    }

    public MoteurFoutuException(string? message) : base(message)
    {
    }

    public MoteurFoutuException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected MoteurFoutuException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}