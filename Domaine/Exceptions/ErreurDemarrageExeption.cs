﻿using System.Runtime.Serialization;

namespace Domaine;

[Serializable]
internal class ErreurDemarrageExeption : Exception
{
    public ErreurDemarrageExeption()
    {
    }

    public ErreurDemarrageExeption(string? message) : base(message)
    {
    }

    public ErreurDemarrageExeption(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected ErreurDemarrageExeption(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}