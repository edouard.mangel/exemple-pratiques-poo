﻿using System;
using System.Runtime.Serialization;

namespace Domaine;

[Serializable]
internal class PanneSecheException : Exception
{
    public PanneSecheException()
    {
    }

    public PanneSecheException(string? message) : base(message)
    {
    }

    public PanneSecheException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected PanneSecheException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}