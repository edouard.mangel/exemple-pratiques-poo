﻿namespace Domaine;

public interface IVehiculeThermique
{
    public void RemplirReservoir();
    public void RemplirReservoir(QuantiteCarburant carburant);
    public void SoufflerEchappement();
}