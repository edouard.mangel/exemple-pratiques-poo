﻿namespace Domaine;

public interface IVehiculeElectrique
{
    public void Recharger();
    public void Recharger(decimal kWH);
}