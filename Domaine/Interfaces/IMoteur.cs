﻿namespace Domaine;

public interface IMoteur
{
    public bool PeutDemarrer();
    public void Demarrer();
    public void Tourner(Pourcentage puissance);
}