﻿using System;

namespace Domaine;


public class RenaultZoe: Voiture, IVehiculeElectrique
{
    private Batterie _batterie { get; set; }    

    public RenaultZoe() : base(new MoteurElectrique())
    {
        _batterie = new Batterie();   
    }

    public override void Accelerer(Pourcentage puissance)
    {
        Moteur.Tourner(puissance);
        decimal consommation = ((MoteurElectrique)Moteur).Rendement * puissance; 
        _batterie.Consommer(consommation);
    }

    public override void Demarrer()
    {
        Moteur.Demarrer();
        Console.WriteLine("La voiture a démarré.");
    }

    public void SoufflerEchappement()
    {
        throw new NotImplementedException();
    }

    public override void Freiner(Pourcentage freinage)
    {
        //TODO, modifier vitesse.
    }

    public void Recharger()
    {
        _batterie.Charger();
    }

    public void Recharger(decimal kWH)
    {
        _batterie.Charger(kWH);
    }
}