﻿using System;

namespace Domaine;


public class RenaultClio : Voiture, IVehiculeThermique
{
    private Reservoir _reservoir { get; set; }    

    public RenaultClio() : base(new MoteurThermique())
    {
        _reservoir = new Reservoir();   
    }

    public RenaultClio(IMoteur moteur) : base(moteur)
    {
        _reservoir = new Reservoir();
    }

    public override void Accelerer(Pourcentage puissance)
    {
        Moteur.Tourner(puissance);
        decimal consommation = ((MoteurThermique)Moteur).Rendement * puissance; 
        _reservoir.Consommer(consommation);

    }

    public override void Demarrer()
    {
        Moteur.Demarrer();
        Console.WriteLine("La voiture a démarré.");
    }

    public void SoufflerEchappement()
    {
        throw new NotImplementedException();
    }

    public override void Freiner(Pourcentage freinage)
    {
        //TODO, modifier vitesse.
    }

    public void RemplirReservoir()
    {
        _reservoir.Remplir();        
    }

    public void RemplirReservoir(QuantiteCarburant carburant)
    {
        _reservoir.Remplir(carburant);
    }
}