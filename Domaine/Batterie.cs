﻿namespace Domaine;

internal class Batterie
{
    private readonly decimal Capacite;
    public decimal NiveauRestant { get; set; }

    internal void Consommer(decimal consommation)
    {
        if (NiveauRestant< consommation)
        {
            throw new BatterieVideException();
        }
        NiveauRestant -= consommation;
    }

    internal void Charger()
    {
        NiveauRestant = Capacite;
    }

    internal void Charger(decimal kWH)
    {
        NiveauRestant = (NiveauRestant + kWH) > Capacite ? Capacite : NiveauRestant + kWH;
    }
}