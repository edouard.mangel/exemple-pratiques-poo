﻿using System;

namespace Domaine;

public class MoteurElectrique : IMoteur
{
    public readonly decimal Rendement;
    public MoteurElectrique()
    {
        Rendement = 0.9M; 
    }

    public void Demarrer()
    {
        if (!PeutDemarrer())
        {
            throw new ErreurDemarrageExeption("La voiture n'a pas pu démarrer.");
        }
        Console.WriteLine("Bzzt");
    }

    public bool PeutDemarrer()
    {
        return true;
    }

    public void Tourner(Pourcentage puissance)
    {
        // usure, etc... 
    }
}