﻿using System;

namespace Domaine;

public class MoteurThermique : IMoteur
{
    public uint NiveauHuile { get; private set; }
    public uint NiveauHuileMinimum => 20;
    public readonly decimal Rendement;
    public MoteurThermique()
    {
        NiveauHuile = 50;
        Rendement = 0.8M; 
    }

    public void Demarrer()
    {
        if (!PeutDemarrer())
        {
            throw new ErreurDemarrageExeption("La voiture n'a pas pu démarrer. Vérifier niveau huile");
        }

        Console.WriteLine("Vroum");
    }

    public bool PeutDemarrer()
    {   
        return NiveauHuile >= NiveauHuileMinimum;
    }

    public void Tourner(Pourcentage puissance)
    {
        Console.WriteLine("broum ");
        NiveauHuile--;
        if (NiveauHuile == 0)
        {
            throw new MoteurFoutuException("le moteur a cramé");
        }
    }
}