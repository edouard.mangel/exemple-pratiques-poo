﻿using System;

namespace Domaine;

internal class Reservoir
{
    private readonly decimal _capaciteLitres;
    public TypeCarburant TypeCarburant { get; set; }
    // Indiquer l'unité évite les confusions. 
    public decimal NiveauLitres { get; private set; }
    public decimal Jauge => NiveauLitres / _capaciteLitres;
    public decimal CapaciteDisponible => _capaciteLitres - NiveauLitres;

    public Reservoir(decimal capacite)
    {
        _capaciteLitres = capacite;
    }

    public Reservoir()
    {
        _capaciteLitres = 50;
    }

    // Respect Tell don't ask, on ne demande pas la capacité, c'est ici qu'on le gère. 
    public void Remplir()
    {
        NiveauLitres = _capaciteLitres;
    }

    public void Remplir(QuantiteCarburant carburant)
    {
        if (carburant.Type != this.TypeCarburant)
        {
            // Early return, programmation défensive
            throw new MauvaisCarburantException();
        }
        NiveauLitres += carburant.Volume;
        if (NiveauLitres > _capaciteLitres) // ça déborde
        {
            NiveauLitres = _capaciteLitres;
        }
    }

    internal void Consommer(decimal volume)
    {
        if (NiveauLitres < volume)
        {
            throw new PanneSecheException();
        }
        NiveauLitres -= volume;
    }
}