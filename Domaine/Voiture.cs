﻿namespace Domaine;

public abstract class Voiture
{
    public abstract void Demarrer();
    public abstract void Accelerer(Pourcentage puissance);
    public abstract void Freiner(Pourcentage freinage);

    // On ne sait pas de quel type, mais on aura besoin de quelque chose qui fait office de moteur
    protected IMoteur Moteur { get; set; }

    protected Voiture(IMoteur moteur)
    {
        Moteur = moteur;
    }    
}