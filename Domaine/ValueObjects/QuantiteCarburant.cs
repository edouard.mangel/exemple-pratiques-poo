﻿namespace Domaine;

public enum TypeCarburant
{
    E85, 
    GPL, 
    SP95, 
    SP98, 
    Gazole
}
public struct QuantiteCarburant
{
    public decimal Volume { get; init; }
    public TypeCarburant Type { get; init; }

    public QuantiteCarburant(TypeCarburant type, decimal quantite) 
    {
        Volume = quantite;
        Type = type;    
    }
}