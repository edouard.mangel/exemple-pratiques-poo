﻿using System;

namespace Domaine;

// Pourquoi utiliser une struc plutôt qu'une classe : https://www.c-sharpcorner.com/Blogs/difference-between-struct-and-class-in-c-sharp
public struct Pourcentage
{
    private readonly decimal _valeur;
    public Pourcentage(decimal valeur)
    {
        // Une bonne raison pour laquelle utiliser un type d'objet custom au lieu d'un int
        // on fait la gestion d'erreur 
        if (valeur <= 0 || valeur > 100)
        {
            throw new ArgumentOutOfRangeException();
        }
        _valeur = valeur;
    }

    public static implicit operator decimal(Pourcentage pa)
    {
        return pa._valeur;
    }
}