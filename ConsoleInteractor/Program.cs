﻿using Domaine; 

// See https://aka.ms/new-console-template for more information

Console.WriteLine("Hello, World!");

RenaultClio voiture1 = new RenaultClio();
voiture1.Demarrer();
RenaultZoe voiture2 = new RenaultZoe();
voiture2.Demarrer();


// D'ici on n'a accès qu'aux classes publiques du domaine,
// pas aux classes internal qui ne nous intéressent pas dans ce contexte. 